## ChexQuestGBA

A quick and dirty hackey fork of GBADoom by Doomhack to let you play Chex Quest.
See the original here: https://github.com/doomhack/GBADoom

It has most everything Chex Quest has, most everything that needed re-coded or transferred over has been done. You should be able to play through quite a bit if not all of the game. I have not done so yet, but I have been able to get through the first two levels so far.

Download the binary here:
http://maomaogames.com/Archive/GBA/ChexQuestGBA-v.09a.gba

Let me know if you have any concerns, bugs, etc.

Great job by him and his compatriots! I just hacked in all the Chex Quest Relevant stuff.

**Bugs:**
On level 2 sometimes it will go into an infinite loop after hitting the exit switch. Have not found time to investigate too much yet.
Until fixed, if this occurs you can, reboot, and use the level skip code and skip ahead. That works.

**What's different?**

- Text strings changed.

- Sounds updated.

- Disabled Episode select.

- Flemoid Monster Behavior should be accurate.

- Barrels do not explode.

- Music Converted to .IT MOD format.

- HUD Statusbar converted.

**What is needed?**
- Remove Monster Infighting

- Status Bar Percent Signs Disappeared.

- Add flag to flip A -> B or B -> A buttons.

- Animated Bootspork End of Level Image.

- Flash Green instead of Red from Rocket, etc.

- Maybe More?

## Cheats:
**Chainsaw:** L, UP, UP, LEFT, L, SELECT, SELECT, UP  
**God mode:** UP, UP, DOWN, DOWN, LEFT, LEFT, RIGHT, RIGHT  
**Ammo & Keys:** L, LEFT, R, RIGHT, SELECT,UP, SELECT, UP  
**Ammo:** R, R, SELECT,R, SELECT,UP, UP, LEFT  
**No Clipping:** UP, DOWN, LEFT, RIGHT, UP, DOWN, LEFT, RIGHT  
**Invincibility:** A, B, L, R, L, R, SELECT, SELECT  
**Berserk:** B, B, R, UP, A, A, R, B  
**Invisibility:** A, A, SELECT,B, A, SELECT, L, B  
**Auto-map:** L, SELECT,R, B, A, R, L, UP  
**Lite-Amp Goggles:** DOWN,LEFT, R, LEFT, R, L, L, SELECT  
**Exit Level:** LEFT,R, LEFT, L, B, LEFT, RIGHT, A  
**Enemy Rockets (Goldeneye):** A, B, L, R, R, L, B, A  
**Toggle FPS counter:** A, B, L, UP, DOWN, B, LEFT, LEFT  

## Controls:  
**Fire:** B  
**Use / Sprint:** A  
**Walk:** D-Pad  
**Strafe:** L & R  
**Automap:** SELECT  
**Weapon up:** A + R  
**Weapon down:** A + L  
**Menu:** Start  

## Building:

To build the GBA version, you will need DevKitArm. The easiest way to get up and running for Windows users is download the installer from here (https://github.com/devkitPro/installer/releases) and install the GBA dev components.

1) Download or Clone ChexQuestGBA source code.
Extract the contents to a folder: (Eg: C:\DevKitPro\Projects\ChexQuestGBA)

1a.) WAD is already setup no need to mess with any WAD files. Chex Quest is freeware so redistributing the WAD should be fine. It is there if you want to play around with the GBA WAD Util and such.

2) Run msys2.bat and type **make**
You may need to edit the msys2.bat with notepad and change the path to go to your real "**msys2\msys2_shell.bat**" file within it if it doesn't work.

3) The project should build ChexQuestGBA.gba and ChexQuestGBA.elf. It will take about 5 minutes or so. You may see a lot of warning messages on the screen. These are normal.

4) Copy ChexQuestGBA.gba (this is the rom file) to your flash cart or run in a emulator.
